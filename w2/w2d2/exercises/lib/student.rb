require 'byebug'

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first, last)
    @first_name, @last_name = first, last
    @courses = []
  end

  def name
    @first_name + ' ' + @last_name
  end

  def enroll(course)
    return if @courses.include?(course)
    check_conflicts(course)
    @courses << course
    course.students << self
  end

  def course_load
    work_load = Hash.new(0)
    @courses.each { |course| work_load[course.department] += course.credits }
    work_load
  end

  def check_conflicts(new_course)
    conflict = @courses.any? { |course| new_course.conflicts_with?(course) }
    raise 'Conflicts has been detected' if conflict
  end
end
