require 'byebug'

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from_tower, to_tower)
    @towers[to_tower] << @towers[from_tower].pop
  end

  def valid_move?(from_tower, to_tower)
    !@towers[from_tower].empty? &&
      (@towers[to_tower].empty? ||
        @towers[from_tower].last < @towers[to_tower].last)
  end

  def won?
    @towers[0].empty? &&
      (@towers[1].count == 3 || @towers[2].count == 3)
  end

  def render
    puts 'Tower 0: ' + design(@towers[0])
    puts 'Tower 1: ' + design(@towers[1])
    puts 'Tower 2: ' + design(@towers[2])
  end

  def design(arr)
    res = []
    design = { 1 => '[#]', 2 => '[##]', 3 => '[###]' }
    arr.each { |el| res << design[el] }
    res.join('-')
  end

  def prompt_user
    print "\nMove from: "
    from = get_int
    print "\nMove to: "
    to = get_int
    [from, to]
  end

  def get_int
    int = Integer(gets.chomp)
    until [0, 1, 2].include?(int)
      print 'please enter valid numbers(0, 1, 2): '
      int = Integer(gets.chomp)
      puts
    end
    int
  end

  def display
    system('clear')
    render
  end

  def check_and_play(from, to)
    if valid_move?(from, to)
      move(from, to)
    else
      puts 'INVALID MOVE!!!'
      sleep(1)
    end
  end

  def play
    until won?
      display
      from, to = prompt_user
      check_and_play(from, to)
    end
  end
end

if $PROGRAM_NAME == __FILE__
  TowersOfHanoi.new.play
end
