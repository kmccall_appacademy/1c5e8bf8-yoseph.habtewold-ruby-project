class Course
  attr_accessor :name, :department, :credits, :students, :days, :time_block

  def initialize(name, dept, credits, days = nil, time = nil)
    @name, @department, @credits = name, dept, credits
    @days, @time_block = days, time
    @students = []
  end

  def add_student(student)
    student.enroll(self)
  end

  def conflicts_with?(course)
    day_conflicts?(course) && time_conflicts?(course)
  end

  def day_conflicts?(course)
    @days.any? { |day| course.days.include?(day) }
  end

  def time_conflicts?(course)
    @time_block == course.time_block
  end

end
