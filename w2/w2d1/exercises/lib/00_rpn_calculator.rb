class RPNCalculator

  def initialize
    @stack = []
  end

  def push(el)
    @stack << el.to_f
  end

  def value
    @stack.last
  end

  def plus
    calculate(:+)
  end

  def minus
    calculate(:-)
  end

  def times
    calculate(:*)
  end

  def divide
    calculate(:/)
  end

  def tokens(rpn_arr)
    tok = '+-*/'.chars
    rpn_arr.split(' ').map do |el|
      tok.include?(el) ? el.to_sym : el.to_i
    end
  end

  def evaluate(rpn_str)
    tokens(rpn_str).each do |el|
      el.is_a?(Integer) ? push(el) : calculate(el)
    end
    value
  end


  private

  def calculate (opp)
    raise "calculator is empty" if @stack.empty?

    case opp
    when :+
      @stack << (@stack.pop + @stack.pop)
    when :-
      second_number = @stack.pop
      @stack << (@stack.pop - second_number)
    when :*
      @stack << (@stack.pop * @stack.pop)
    when :/
      second_number = @stack.pop
      @stack << (@stack.pop / second_number)
    else
      raise "invalid opperator"
    end
  end

end
