require 'byebug'
class Integer
  ONES = {
    0 => 'zero',
    1 => 'one',
    2 => 'two',
    3 => 'three',
    4 => 'four',
    5 => 'five',
    6 => 'six',
    7 => 'seven',
    8 => 'eight',
    9 => 'nine'
  }

  TEENS = {
    10 => 'ten',
    11 => 'eleven',
    12 => 'twelve',
    13 => 'thirteen',
    14 => 'fourteen',
    15 => 'fifteen',
    16 => 'sixteen',
    17 => 'seventeen',
    18 => 'eighteen',
    19 => 'nineteen'
  }

  TENS = {
    20 => 'twenty',
    30 => 'thirty',
    40 => 'forty',
    50 => 'fifty',
    60 => 'sixty',
    70 => 'seventy',
    80 => 'eighty',
    90 => 'ninety'
  }

  MAGNITUDES = {
    100 => 'hundred',
    1000 => 'thousand',
    1_000_000 => 'million',
    1_000_000_000 => 'billion',
    1_000_000_000_000 => 'trillion'
  }
  def in_words
    if self < 10
      ONES[self]
    elsif self < 20
      TEENS[self]
    elsif self < 100
      eval_upto_100(self)
    else
      mag = find_magnitude
      evaluate(self, mag)
    end
  end

  def eval_upto_100(int)
    return TENS[int] if TENS.key?(int)
    (int - (int % 10)).in_words + ' ' + (int % 10).in_words
  end

  def evaluate(int, mag)
    int % mag != 0 ? inperfect_num(int, mag) : perfect_num(int, mag)
  end

  def inperfect_num(int, mag)
    (int / mag).in_words + ' ' + MAGNITUDES[mag] + ' ' + (int % mag).in_words
  end

  def perfect_num(int, mag)
    (int / mag).in_words + ' ' + MAGNITUDES[mag]
  end

  def find_magnitude
    MAGNITUDES.select { |k| self >= k }.max.first
  end
end
