# uncomment line 21 before playing on the command line
require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current

  def initialize(p1, p2)
    @player_one = p1
    @player_one.mark = :X
    @player_two = p2
    @player_two.mark = :O
    @board = Board.new
    @current = @player_one
  end

  def play_turn
    @current.display(@board)
    move = @current.get_move
    # move = @current.get_move until valid_move?(move)
    @board.place_mark(move, @current.mark)
    @current.display(@board)
    switch_players!
  end

  def valid_move?(pos)
    puts 'ILLIGAL MOVE!' if @board[pos] != nil
    @board[pos] == nil
  end

  def current_player
    @current
  end

  def switch_players!
    if @current == @player_one
      @current = @player_two
    else
      @current = @player_one
    end
  end

  def play
    loop do
      play_turn
      break if @board.over?
    end
    @current.display(@board)
    display_winner
  end

  def display_winner
    if @board.winner
      system("say congratulations #{@board.winner}")
      puts "#{@board.winner} WON!"
    else
      puts "IT'S A TIE!!"
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  puts 'Enter name: '
  name = gets.chomp
  p1 = HumanPlayer.new(name)
  p2 = ComputerPlayer.new('computer')
  Game.new(p1, p2).play
end
