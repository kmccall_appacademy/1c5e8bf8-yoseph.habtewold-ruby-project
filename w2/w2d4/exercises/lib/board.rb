class Board
  attr_accessor :grid

  def initialize(grid=nil)
    grid ? @grid = grid : @grid = Array.new(3){ Array.new(3) }
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos] == nil
  end

  def winner
    row_winner || col_winner || diag_winner
  end

  def over?
    winner ? true : @grid.flatten.all?
  end

  private

  def row_winner
    result = matcing_row(@grid)
    result.empty? ? nil : result[0][0]
  end

  def col_winner
    result = matcing_row(@grid.transpose)
    result.empty? ? nil : result[0][0]
  end

  def diag_winner
    diag_left = [self[[0,0]], self[[1,1]], self[[2,2]]]
    diag_right = [self[[0,2]], self[[1,1]], self[[2,0]]]

    if !diag_left[0].nil? && diag_left.uniq.count == 1
      return diag_left[0]
    elsif !diag_right[0].nil? && diag_right.uniq.count == 1
      return diag_right[0]
    end
    nil
  end

  def matcing_row(row)
    row.select do |triple|
      !triple[0].nil? && triple.uniq.count == 1
    end
  end
end
