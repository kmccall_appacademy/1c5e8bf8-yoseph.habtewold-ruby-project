class HumanPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts 'where do you want to move :'
    move = gets.chomp
    [move[0].to_i, move[-1].to_i]
  end

  def display(board)
    @board = board
    system('clear')
    puts disp_line(board.grid[0])
    puts '-------------'
    puts disp_line(board.grid[1])
    puts '-------------'
    puts disp_line(board.grid[2])
  end

  def disp_line(triplet)
    "  #{triplet[0] || ' '} | #{triplet[1] || ' '} | #{triplet[2] || ' '}"
  end
end
