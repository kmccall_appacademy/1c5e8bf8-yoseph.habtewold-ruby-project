class ComputerPlayer
  attr_accessor :name, :board, :mark


  def initialize(name)
    @name = name
    @board = Board.new
  end

  def display(board)
    @board = board
  end

  def get_move
    moves = open_moves
    move = moves.select { |pos| wining_move?(pos) }
    return moves.sample if move == []
    move.first
  end

  def open_moves
    moves = []
    (0..2).each do |row|
      (0..2).each do |col|
        moves << [row, col] if @board[[row, col]].nil?
      end
    end
    moves
  end

  def wining_move?(pos)
    @board[pos] = @mark
    if @board.winner == @mark
      @board[pos] = nil
      true
    else
      @board[pos] = nil
      false
    end
  end
end
