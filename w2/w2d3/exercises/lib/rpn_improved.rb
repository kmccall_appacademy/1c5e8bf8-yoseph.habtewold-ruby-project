#!/usr/bin/env ruby
require 'byebug'
require '../../../w2d1/exercises/lib/00_rpn_calculator.rb'

class RPNCalculator

  def eveluate_file(file_name)
    File.foreach(file_name) do |line|
      puts evaluate(line.chomp)
    end
  end

  def user_interface
    input_arr = []

    loop do
      print '>> '
      op = gets
      op != "\n" ? input_arr << add(op) : break
    end
    puts "  =====> #{evaluate(input_arr.join(' '))} <====="
  end

  def add(el)
    operators = '+-*/'.chars
    operators.include?(el.chomp) ? el.chomp : el.chomp.to_i
  end
end

if __FILE__ == $PROGRAM_NAME
  calculator = RPNCalculator.new
  ARGV.empty? ? calculator.user_interface : calculator.eveluate_file(ARGV[0])
end
