def guessing_game
  target = rand(1..100)

  count = 0
  loop do
    guess = user_guess
    count += 1
    break if evaluate_guess(guess, target) == -1
  end
  puts "You complited in #{count} tries."
end

def evaluate_guess(guess, target)
  if guess < target
    puts "#{guess} is too low!"
  elsif guess == target
    puts "#{guess} is CORRECT!!"
    return -1
  elsif guess > target
    puts "#{guess} is too high!"
  end
end

def user_guess
  puts 'Guess a number!'
  gets.to_i
end

def file_shuffel(file_name)
  contents = File.readlines(file_name)
  shuffled = contents.shuffle

  name = File.basename(file_name, ".*")
  f = File.open("#{name}-shuffled.txt", "w")
  shuffled.each { |line| f.puts line.chomp }
  f.close
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.empty?
    puts 'Input file name: '
    file_name = gets.chomp
    file_shuffel(file_name)
  else
    file_shuffel(ARGV[0])
  end
end
