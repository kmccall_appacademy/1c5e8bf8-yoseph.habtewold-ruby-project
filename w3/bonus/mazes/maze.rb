# parse initial maze file
# get start location
# get finsh location
# get open location

require 'byebug'
class Maze
  attr_accessor :board

  def initialize(maze)
    @board = parse(maze)
    @board.map! { |l| l.chomp.chars }
  end

  def parse(file)
    f = File.open(file)
    f.readlines
  end

  def display
    @board.each do |line|
      puts line.join
    end
  end

  def [](pos)
    row, col = pos
    @board[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @board[row][col] = mark
  end

  def find(char)
    @board.each_with_index do |row, i|
      row.each_with_index do |el, j|
        return [i, j] if char == el
      end
    end
    nil
  end
end # end of class

















if __FILE__ == $PROGRAM_NAME
  m = Maze.new(ARGV[0])
  m.display
  p m.find('S')
end
