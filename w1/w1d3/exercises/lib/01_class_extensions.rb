
class String
  def caesar(shift)
    ciphered = ''

    self.each_char do |ch|
      new = (ch.ord + shift) % 'z'.ord
      if new < 'a'.ord
        ciphered += (new + ('a'.ord - 1)).chr
      else
        ciphered += new.chr
      end
    end
    ciphered
  end
end

class Hash
  def difference(other_hash)
    res = {}
    self.each { |key,val| res[key] = val if !other_hash.include?(key) }
    other_hash.each { |key,val| res[key] = val if !self.include?(key) }
    res
  end
end

class Fixnum
  def stringify(base)
    res = []
    pow = 0
    map = '0123456789abcdef'.chars

    until self / (base ** pow) == 0
      idx = (self / (base ** pow)) % base
      res.unshift(map[idx])
      pow += 1
    end
    res.join.to_s
  end
end
