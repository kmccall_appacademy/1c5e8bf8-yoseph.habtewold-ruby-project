# require 'byebug'
VOWELS = ['a','e','i','o','u']

def translate(words)
  words.split.map{|wrd| latinize(wrd)}.join(' ')
end

def latinize(word)
  until VOWELS.include?(word[0])
    if word[0..1] == 'qu'
      word = word[2..-1] + 'qu'
    else
      word = word[1..-1] + word[0]
    end 
  end
  word + 'ay'
end
