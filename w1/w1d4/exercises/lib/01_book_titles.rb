class Book
  attr_accessor :title

  EXP = ["the", "and", "over", "or", "to", "a",
         "but", "by", "for", "in", "of","an"]

  def title= (phrase)
    @title = phrase.split.map.with_index do |wrd, i|
      if EXP.include?(wrd) and i != 0
        wrd
      else
        wrd.capitalize
      end
    end.join(' ')
  end

end
