class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(definition)
    if definition.class == String
      @entries[definition] = nil
    elsif definition.class == Hash
      @entries.merge!(definition)
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    keywords.include?(key)
  end

  def find(word)
    res = {}
    @entries.each {|key, defn| res[key] = defn if key.include? word}
    res
  end

  def printable
    prn = ''
    sorted = @entries.sort_by { |k,v| k }

    sorted.each {|k,v| prn += "[#{k}] \"#{v}\"\n" }
    prn[0..-2]
  end
end
