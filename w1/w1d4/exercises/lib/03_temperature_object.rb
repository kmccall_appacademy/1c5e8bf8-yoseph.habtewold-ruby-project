class Temperature
  attr_accessor :fahrenheit, :celsius

  def initialize(options)
    if options.keys.include?(:f)
      @fahrenheit = options[:f]
      @celsius = ftoc(@fahrenheit)
    elsif options.keys.include?(:c)
      @celsius = options[:c]
      @fahrenheit = ctof(@celsius)
    end
  end

  def in_fahrenheit
    @fahrenheit
  end

  def in_celsius
    @celsius
  end

  def ftoc (temp)
    (temp - 32) * (5.0/9.0)
  end

  def ctof (temp)
    temp * (9.0 / 5.0) + 32
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end
end

class Celsius < Temperature
  def initialize(temp)
    super({:c => temp})
  end
end

class Fahrenheit <  Temperature
  def initialize(temp)
    super({:f => temp})
  end
end
