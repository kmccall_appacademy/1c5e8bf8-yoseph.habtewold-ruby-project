require 'byebug'

def factors(num)
  (1..num).select {|i| num % i == 0}
end

def substrings(string)
  subs = []
  len = string.length
  (0...len).each do |first|
    (0...len).each do |last|
      subs << string[first..last] if !subs.include? string[first..last]
    end
  end
  subs
end

def subwords(word, dictionary)
  words =[]
  substrings(word).each {|wrd| words << wrd if dictionary.include? wrd}
  words
end

def doubler(array)
  array.map{|num| num*2 }
end

class Array
  def bubble_sort!
    sorted = false

    until sorted
      sorted = true
      (0...self.length).each do |i|
        next if i == self.length - 1
        if self[i] > self[i+1]
          self[i], self[i+1] = self[i+1], self[i]
          sorted = false
        end
      end
    end
    self
  end

  def bubble_sort!(&prc)
    sorted = false
    prc ||= Proc.new {|x,y| x <=> y}

    until sorted
      sorted = true
      (0...self.length).each do |i|
        next if i == self.length - 1
        if prc.call(self[i],self[i+1]) == 1
          self[i], self[i+1] = self[i+1], self[i]
          sorted = false
        end
      end
    end
    self
  end

  def bubble_sort(&prc)
    self.dup.bubble_sort!
  end

  def my_each
    i=0
    while i < self.length
      yield self[i]
      i += 1
    end
    self
  end

  def my_map
    res = []
    self.my_each{|el| res << yield(el)}
    res
  end

  def my_select
    res = []
    self.my_each{|el| res << el if yield(el)}
    res
  end

  def my_inject
    acc = self[0]
    self[1..-1].my_each{|el| acc = yield(acc, el)}
    acc
  end
end

def concatenate(strings)
  strings.inject(:+)
end
