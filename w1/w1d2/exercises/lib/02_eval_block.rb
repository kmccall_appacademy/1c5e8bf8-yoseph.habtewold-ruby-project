def eval_block(*args)
  if block_given?
    yield *args
  else
    raise "NO BLOCK GIVEN"
  end
end
